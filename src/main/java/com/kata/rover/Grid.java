package com.kata.rover;

import com.kata.rover.exceptions.GridLengthException;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Grid {

    private final BigInteger length;
    private final List<Point> obstacles;

    public Grid(BigInteger length) {
        if (length.compareTo(BigInteger.ONE) < 0)
            throw new GridLengthException("Grid length should not be below 1.");
        this.length = length;
        this.obstacles = new ArrayList<>();
    }

    public Grid addObstacle(Point obstacle) {
        obstacles.add(obstacle);
        return this;
    }

    public boolean isPointOnObstacle(Point point){
        return obstacles.contains(point);
    }

    public BigInteger getLength() {
        return length;
    }
}
