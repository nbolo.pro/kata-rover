package com.kata.rover;

import com.kata.rover.commands.Command;
import com.kata.rover.commands.CommandExecutor;
import com.kata.rover.exceptions.GridObstacleException;

import java.math.BigInteger;
import java.util.Optional;

public class PositionService {

    private final static BigInteger INITIAL_XY_POSITION = BigInteger.ZERO;
    private final Grid grid;
    private final ReportingService reportingService;

    public PositionService(Grid grid, ReportingService reportingService) {
        this.grid = grid;
        this.reportingService = reportingService;
    }

    public Position computeNextPosition(String commands, Position roverPosition) {
        try {
            for (char command : commands.toCharArray()) {
                Optional<CommandExecutor> executorFromChar = Command.getExecutorFromChar(command);
                if (!executorFromChar.isPresent()) {
                    continue;
                }
                Position nextUnadjustedPosition = executorFromChar.get().executeCommand(roverPosition);
                roverPosition = adjustToGrid(nextUnadjustedPosition);
            }
        } catch (GridObstacleException e) {
            reportingService.reportObstacle(e.getMessage());
        }
        return roverPosition;
    }

    public Position adjustToGrid(Position position) throws GridObstacleException {
        Point point = position.getPoint();
        Point newPoint = point;

        if (point.getxPosition().compareTo(grid.getLength()) > 0) {
            newPoint = new Point(INITIAL_XY_POSITION, point.getyPosition());
        }
        if (point.getxPosition().compareTo(INITIAL_XY_POSITION) < 0) {
            newPoint = new Point(grid.getLength(), point.getyPosition());
        }
        if (point.getyPosition().compareTo(INITIAL_XY_POSITION) < 0) {
            newPoint = new Point(point.getxPosition(), grid.getLength());
        }
        if (point.getyPosition().compareTo(grid.getLength()) > 0) {
            newPoint = new Point(point.getxPosition(), INITIAL_XY_POSITION);
        }
        if (grid.isPointOnObstacle(newPoint)) {
            throw new GridObstacleException(newPoint);
        }

        return new Position(position.getDirection(), newPoint);
    }
}
