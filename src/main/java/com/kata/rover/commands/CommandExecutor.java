package com.kata.rover.commands;

import com.kata.rover.Position;

@FunctionalInterface
public interface CommandExecutor {
    Position executeCommand(Position position);
}
