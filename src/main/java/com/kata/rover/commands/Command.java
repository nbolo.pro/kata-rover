package com.kata.rover.commands;

import java.util.Arrays;
import java.util.Optional;

public enum Command {
    FORWARD('f', new ForwardCommandExecutor()),
    BACKWARD('b', new BackwardCommandExecutor()),
    LEFT('l', new TurnLeftCommandExecutor()),
    RIGHT('r', new TurnRightCommandExecutor());

    private final char commandCharacter;
    private final CommandExecutor commandExecutor;

    Command(char commandCharacter, CommandExecutor commandExecutor) {
        this.commandCharacter = commandCharacter;
        this.commandExecutor = commandExecutor;
    }

    public char getCommandCharacter() {
        return commandCharacter;
    }

    public CommandExecutor getCommandExecutor() {
        return commandExecutor;
    }

    public static Optional<CommandExecutor> getExecutorFromChar(char commandCharacter) {
        return Arrays.stream(Command.values())
                .filter(command -> command.getCommandCharacter() == commandCharacter)
                .map(Command::getCommandExecutor)
                .findFirst();
    }
}
