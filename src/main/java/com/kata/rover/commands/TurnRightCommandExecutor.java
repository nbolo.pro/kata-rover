package com.kata.rover.commands;

import com.kata.rover.Position;
import com.kata.rover.Direction;

public class TurnRightCommandExecutor implements CommandExecutor {
    @Override
    public Position executeCommand(Position position) {
        Direction direction = position.getDirection().rightTurn();
        return new Position(direction, position.getPoint());
    }
}
