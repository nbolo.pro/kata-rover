package com.kata.rover.commands;

import com.kata.rover.Point;
import com.kata.rover.Position;

public class BackwardCommandExecutor implements CommandExecutor {

    @Override
    public Position executeCommand(Position position) {
        Point point = position.getDirection().backwardIncrement();
        return position.incrementPoint(point);
    }
}
