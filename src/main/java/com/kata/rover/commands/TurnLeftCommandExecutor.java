package com.kata.rover.commands;

import com.kata.rover.Position;
import com.kata.rover.Direction;

public class TurnLeftCommandExecutor implements CommandExecutor {
    @Override
    public Position executeCommand(Position position) {
        Direction direction = position.getDirection().leftTurn();
        return new Position(direction, position.getPoint());
    }
}
