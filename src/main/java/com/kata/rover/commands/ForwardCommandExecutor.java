package com.kata.rover.commands;

import com.kata.rover.Point;
import com.kata.rover.Position;

public class ForwardCommandExecutor implements CommandExecutor {

    @Override
    public Position executeCommand(Position position) {
        Point point = position.getDirection().forwardIncrement();
        return position.incrementPoint(point);
    }
}
