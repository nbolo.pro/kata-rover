package com.kata.rover;

public enum Direction {

    NORTH(new Point(0, 1), new Point(0, -1)) {
        @Override
        public Direction leftTurn() {
            return Direction.WEST;
        }

        @Override
        public Direction rightTurn() {
            return Direction.EAST;
        }
    },

    SOUTH(new Point(0, -1), new Point(0, 1)) {
        @Override
        public Direction leftTurn() {
            return Direction.EAST;
        }

        @Override
        public Direction rightTurn() {
            return Direction.WEST;

        }
    },

    EAST(new Point(1, 0), new Point(-1,0)) {
        @Override
        public Direction leftTurn() {
            return Direction.NORTH;
        }

        @Override
        public Direction rightTurn() {
            return Direction.SOUTH;
        }
    },

    WEST(new Point(-1, 0), new Point(1,0)) {
        @Override
        public Direction leftTurn() {
            return Direction.SOUTH;
        }

        @Override
        public Direction rightTurn() {
            return Direction.NORTH;
        }
    };

    private final Point forwardIncrement;
    private final Point backwardIncrement;

    Direction(Point forwardIncrement, Point backwardIncrement) {
        this.forwardIncrement = forwardIncrement;
        this.backwardIncrement = backwardIncrement;
    }

    public Point forwardIncrement() {
        return forwardIncrement;
    }

    public Point backwardIncrement() {
        return backwardIncrement;
    }

    public abstract Direction leftTurn();

    public abstract Direction rightTurn();

    @Override
    public String toString() {
        return this.name().substring(0,1);
    }
}
