package com.kata.rover;

public class ReportingService {
    public void reportObstacle(String message) {
        System.err.println(message);
    }
}
