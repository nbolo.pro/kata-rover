package com.kata.rover.exceptions;

import com.kata.rover.Point;

public class GridObstacleException extends Throwable {

    public static final String ERROR_MESSAGE = "Rover encountered an obstacle on Point : ";

    public GridObstacleException(Point obstaclePoint) {
        super(ERROR_MESSAGE + obstaclePoint.formatPoint());
    }
}
