package com.kata.rover.exceptions;

public class GridLengthException extends RuntimeException {
    public GridLengthException(String s) {
        super(s);
    }
}
