package com.kata.rover;

import java.math.BigInteger;
import java.util.Objects;

public class Point {

    public static final Point ORIGIN_POINT = new Point(0, 0);

    private final BigInteger xPosition;
    private final BigInteger yPosition;

    public Point(BigInteger xPosition, BigInteger yPosition) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public Point(int xPosition, int yPosition) {
        this.xPosition = new BigInteger(String.valueOf(xPosition));
        this.yPosition = new BigInteger(String.valueOf(yPosition));
    }

    public BigInteger getxPosition() {
        return xPosition;
    }

    public BigInteger getyPosition() {
        return yPosition;
    }

    public Point increment(Point point) {
        return new Point(point.getxPosition().add(this.xPosition), point.getyPosition().add(this.yPosition));
    }

    public String formatPoint() {
        return this.xPosition.toString() + ":" + this.yPosition.toString();
    }

    @Override
    public String toString() {
        return formatPoint();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Objects.equals(xPosition, point.xPosition) && Objects.equals(yPosition, point.yPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(xPosition, yPosition);
    }
}
