package com.kata.rover;

import java.util.Objects;

public class Position {

    private final Direction direction;
    private final Point point;

    public Position(Direction facingDirection, Point currentPosition) {
        this.direction = facingDirection;
        this.point = currentPosition;
    }

    public Direction getDirection() {
        return direction;
    }

    public Point getPoint() {
        return point;
    }

    public Position changeDirection(Direction direction) {
        return new Position(direction, this.point);
    }

    public Position incrementPoint(Point point) {
        return new Position(this.direction, this.point.increment(point));
    }

    public String formatPosition() {
        return point.formatPoint() + ":" + direction.toString();
    }

    @Override
    public String toString() {
        return formatPosition();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Objects.equals(direction, position.direction) && Objects.equals(point, position.point);
    }

    @Override
    public int hashCode() {
        return Objects.hash(direction, point);
    }
}
