package com.kata.rover;

public class Rover {

    private final PositionService positionService;
    private Position position;
    private String commands = "";

    public Rover(Position position, Grid grid) {
        this.positionService = new PositionService(grid, new ReportingService());
        this.position = position;
    }

    public Rover(Position position, Grid grid, ReportingService reportingService) {
        this.position = position;
        this.positionService = new PositionService(grid, reportingService);
    }

    public void receiveCommands(String commands) {
        if (commands != null) {
            this.commands = commands;
        }
    }

    public void executeCommands() {
        position = positionService.computeNextPosition(commands, position);
    }

    public void changeDirection(Direction direction) {
        this.position = position.changeDirection(direction);
    }

    public String getPosition() {
        return this.position.formatPosition();
    }

    public void clearCommands() {
        this.commands = "";
    }
}
