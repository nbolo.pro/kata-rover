import com.kata.rover.*;
import com.kata.rover.Direction;
import com.kata.rover.exceptions.GridObstacleException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

class RoverTest {

    private static final Grid FIVE_LENGTH_GRID = new Grid(new BigInteger("5"));

    @Test
    @DisplayName("When creating a rover should initialize a rover correctly.")
    void init_rover_correctly() {
        Rover rover = initRover(2, 3, Direction.NORTH);
        assertThat(rover.getPosition()).isEqualTo("2:3:N");

        rover = initRover(0, 0, Direction.SOUTH);
        assertThat(rover.getPosition()).isEqualTo("0:0:S");

        rover = initRover(-3, 6, Direction.EAST);
        assertThat(rover.getPosition()).isEqualTo("-3:6:E");

        rover = initRover(Integer.MAX_VALUE, Integer.MAX_VALUE, Direction.WEST);
        assertThat(rover.getPosition()).isEqualTo(Integer.MAX_VALUE + ":" + Integer.MAX_VALUE + ":W");
    }

    private Rover initRover(int xPoint, int yPoint, Direction direction) {
        Point currentPosition = new Point(new BigInteger(String.valueOf(xPoint)), new BigInteger(String.valueOf(yPoint)));
        Position position = new Position(direction, currentPosition);
        Grid grid = new Grid(new BigInteger(String.valueOf(Integer.MAX_VALUE)));
        return new Rover(position, grid);
    }

    private Rover initRover(int xPoint, int yPoint, Direction direction, Grid grid) {
        Position position = new Position(direction, new Point(new BigInteger(String.valueOf(xPoint)), new BigInteger(String.valueOf(yPoint))));
        return new Rover(position, grid);
    }

    @Test
    @DisplayName("When rover is moving forward toward N should increase y position")
    void moving_rover_forward_north() {
        Rover rover = initRover(0, 0, Direction.NORTH);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:1:N");
        rover.clearCommands();

        rover.receiveCommands("ff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:3:N");
        rover.clearCommands();

        rover.receiveCommands("fffffffffffffff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:18:N");
    }


    @Test
    @DisplayName("When rover is moving forward toward E should increase x position")
    void moving_rover_forward_east() {
        Rover rover = initRover(0, 0, Direction.EAST);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("1:0:E");
        rover.clearCommands();

        rover.receiveCommands("ff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("3:0:E");
        rover.clearCommands();

        rover.receiveCommands("fffffffffffffff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("18:0:E");
    }

    @Test
    @DisplayName("When rover is moving forward toward W should decrease x position")
    void moving_rover_forward_west() {
        Rover rover = initRover(20, 0, Direction.WEST);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("19:0:W");
        rover.clearCommands();

        rover.receiveCommands("ff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("17:0:W");
        rover.clearCommands();

        rover.receiveCommands("fffffffffffffff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("2:0:W");
    }

    @Test
    @DisplayName("When rover is moving forward toward S should decrease y position")
    void moving_rover_forward_south() {
        Rover rover = initRover(0, 20, Direction.SOUTH);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:19:S");
        rover.clearCommands();

        rover.receiveCommands("ff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:17:S");
        rover.clearCommands();

        rover.receiveCommands("fffffffffffffff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:2:S");
    }

    @Test
    @DisplayName("When executing no command should not move")
    void not_moving_rover() {
        Rover rover = initRover(0, 0, Direction.SOUTH);
        assertThat(rover.getPosition()).isEqualTo("0:0:S");
    }


    @Test
    @DisplayName("When sending wrong commands should keep executing correctly")
    void wrong_command() {
        Rover rover = initRover(0, 0, Direction.SOUTH);
        rover.receiveCommands("bbbbfabafxf");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:2:S");
    }

    @Test
    @DisplayName("When going over int limit should come back to initial point")
    void int_limit_edges() {
        Rover rover = initRover(Integer.MAX_VALUE, 0, Direction.EAST, new Grid(new BigInteger(String.valueOf(Integer.MAX_VALUE))));
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:0:E");
    }

    @Test
    @DisplayName("When moving rover back and forth and changing directions should keep right position")
    void complex_moving_rover_test() {
        Rover rover = initRover(0, 0, Direction.NORTH);

        rover.receiveCommands("ffbfffffbffbffbfbbf");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:7:N");
        rover.clearCommands();

        rover.changeDirection(Direction.WEST);
        rover.receiveCommands("bfbffbbbbbff");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("2:7:W");
        rover.clearCommands();

        rover.changeDirection(Direction.NORTH);
        rover.receiveCommands("bbffbbfffffffffffbbb");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("2:13:N");
        rover.clearCommands();

        rover.changeDirection(Direction.SOUTH);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("2:12:S");
        rover.clearCommands();

        rover.changeDirection(Direction.EAST);
        rover.receiveCommands("ffbbffbbbbbbffffffffffffbbbbbbfb");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("4:12:E");
        rover.clearCommands();
    }

    @Test
    @DisplayName("When rover goes over the edge over x axis should start again from 0.")
    void come_back_on_x_axis() {
        Rover rover = initRover(5, 0, Direction.EAST, FIVE_LENGTH_GRID);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:0:E");
    }

    @Test
    @DisplayName("When rover goes over the edge over x axis from 1 should start again from edge limit.")
    void come_back_on_x_axis_from_edge() {
        Rover rover = initRover(0, 0, Direction.WEST, FIVE_LENGTH_GRID);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("5:0:W");
    }

    @Test
    @DisplayName("When rover goes over the edge over y axis from 1, y position should back to 5")
    void come_back_on_y_axis_inverted() {
        Rover rover = initRover(0, 0, Direction.SOUTH, FIVE_LENGTH_GRID);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:5:S");
    }

    @Test
    @DisplayName("When rover goes over the edge over y axis from 5, y position should back to 1")
    void come_back_on_y_axis_from_edge() {
        Rover rover = initRover(0, 5, Direction.NORTH, FIVE_LENGTH_GRID);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("0:0:N");
    }

    @Test
    @DisplayName("When rover encounter an obstacle should stay at last possible position")
    void encounter_obstacle_stay_last_position() {
        Grid grid = new Grid(new BigInteger("10"))
                .addObstacle(new Point(1, 1));

        Rover rover = initRover(0, 0, Direction.NORTH, grid);
        rover.receiveCommands("f");
        rover.executeCommands();
        rover.changeDirection(Direction.EAST);
        rover.executeCommands();

        assertThat(rover.getPosition()).isEqualTo("0:1:E");
    }

    @Test
    @DisplayName("When rover encounter an obstacle should report it")
    void encounter_obstacle_report_obstacle() {
        Point obstacle = new Point(1, 1);
        Grid grid = new Grid(BigInteger.TEN)
                .addObstacle(obstacle);
        ReportingService reportingService = Mockito.mock(ReportingService.class);
        Rover rover = new Rover(new Position(Direction.NORTH, Point.ORIGIN_POINT), grid, reportingService);
        rover.receiveCommands("f");
        rover.executeCommands();
        rover.changeDirection(Direction.EAST);
        rover.executeCommands();

        Mockito.verify(reportingService, Mockito.times(1))
                .reportObstacle(GridObstacleException.ERROR_MESSAGE + obstacle.formatPoint());
    }

    @Test
    @DisplayName("When rover encounter an obstacle at the edge of the planet")
    void rover_should_keep_initial_position() {
        Grid grid = new Grid(new BigInteger("10"))
                .addObstacle(new Point(5, 0));
        Position initialPosition = new Position(Direction.NORTH, new Point(5, 10));
        Rover rover = new Rover(initialPosition, grid);
        rover.receiveCommands("f");
        rover.executeCommands();
        assertThat(rover.getPosition()).isEqualTo("5:10:N");
    }
}
