import com.kata.rover.Grid;
import com.kata.rover.exceptions.GridLengthException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class GridTest {

    @Test
    @DisplayName("When a grid is created, it's length should never be lower than 1")
    void negativeGrid() {
        assertThrows(GridLengthException.class, () -> new Grid(new BigInteger("-1")));
        assertThrows(GridLengthException.class, () -> new Grid(BigInteger.ZERO));
    }

}
