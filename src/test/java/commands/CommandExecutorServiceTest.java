package commands;

import com.kata.rover.Point;
import com.kata.rover.Position;
import com.kata.rover.commands.Command;
import com.kata.rover.Direction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CommandExecutorServiceTest {

    public static final char LEFT_COMMAND = 'l';
    public static final char RIGHT_COMMAND = 'r';
    private static final char BACKWARD_COMMAND = 'b';
    private static final char FORWARD_COMMAND = 'f';

    @Test
    @DisplayName("When received command is f with N Direction should increase y axis by 1")
    void increase_y_axis_going_forward_with_north_direction() {
        Position originPosition = new Position(Direction.NORTH, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.NORTH, new Point(0, 1));
        Position actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.NORTH, new Point(7, 10));
        expectedPosition = new Position(Direction.NORTH, new Point(7, 11));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.NORTH, new Point(100, 56));
        expectedPosition = new Position(Direction.NORTH, new Point(100, 57));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is f with S Direction should decrease y axis by 1")
    void decrease_y_axis_going_forward_with_south_direction() {
        Position originPosition = new Position(Direction.SOUTH, new Point(0, 1));
        Position expectedPosition = new Position(Direction.SOUTH, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.SOUTH, new Point(7, 10));
        expectedPosition = new Position(Direction.SOUTH, new Point(7, 9));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.SOUTH, new Point(100, 56));
        expectedPosition = new Position(Direction.SOUTH, new Point(100, 55));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is f with E Direction should increase x axis by 1")
    void increase_x_axis_going_forward_with_east_direction() {
        Position originPosition = new Position(Direction.EAST, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.EAST, new Point(1, 0));
        Position actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.EAST, new Point(7, 10));
        expectedPosition = new Position(Direction.EAST, new Point(8, 10));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.EAST, new Point(101, 56));
        expectedPosition = new Position(Direction.EAST, new Point(102, 56));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is f with W Direction should decrease x axis by 1")
    void decrease_x_axis_going_forward_with_west_direction() {
        Position originPosition = new Position(Direction.WEST, new Point(1, 0));
        Position expectedPosition = new Position(Direction.WEST, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.WEST, new Point(7, 10));
        expectedPosition = new Position(Direction.WEST, new Point(6, 10));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.WEST, new Point(101, 56));
        expectedPosition = new Position(Direction.WEST, new Point(100, 56));
        actualPosition = Command.getExecutorFromChar(FORWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }


    //BACKWARD
    @Test
    @DisplayName("When received command is b with S Direction should increase y axis by 1")
    void increase_y_axis_going_backward_with_south_direction() {
        Position originPosition = new Position(Direction.SOUTH, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.SOUTH, new Point(0, 1));
        Position actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.SOUTH, new Point(7, 10));
        expectedPosition = new Position(Direction.SOUTH, new Point(7, 11));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.SOUTH, new Point(100, 56));
        expectedPosition = new Position(Direction.SOUTH, new Point(100, 57));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is b with N Direction should decrease y axis by 1")
    void decrease_y_axis_going_backward_with_north_direction() {
        Position originPosition = new Position(Direction.NORTH, new Point(0, 1));
        Position expectedPosition = new Position(Direction.NORTH, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.NORTH, new Point(7, 10));
        expectedPosition = new Position(Direction.NORTH, new Point(7, 9));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.NORTH, new Point(100, 56));
        expectedPosition = new Position(Direction.NORTH, new Point(100, 55));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is b with W Direction should increase x axis by 1")
    void increase_x_axis_going_backward_with_west_direction() {
        Position originPosition = new Position(Direction.WEST, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.WEST, new Point(1, 0));
        Position actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.WEST, new Point(7, 10));
        expectedPosition = new Position(Direction.WEST, new Point(8, 10));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.WEST, new Point(101, 56));
        expectedPosition = new Position(Direction.WEST, new Point(102, 56));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is b with W Direction should decrease x axis by 1")
    void decrease_x_axis_going_backward_with_east_direction() {
        Position originPosition = new Position(Direction.EAST, new Point(1, 0));
        Position expectedPosition = new Position(Direction.EAST, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.EAST, new Point(7, 10));
        expectedPosition = new Position(Direction.EAST, new Point(6, 10));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);

        originPosition = new Position(Direction.EAST, new Point(101, 56));
        expectedPosition = new Position(Direction.EAST, new Point(100, 56));
        actualPosition = Command.getExecutorFromChar(BACKWARD_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is l with N Direction should be directed W")
    void l_command_N_direction_end_W() {
        Position originPosition = new Position(Direction.NORTH, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.WEST, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(LEFT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is l with S Direction should be directed E")
    void l_command_S_direction_end_E() {
        Position originPosition = new Position(Direction.SOUTH, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.EAST, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(LEFT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is l with W Direction should be directed S")
    void l_command_W_direction_end_S() {
        Position originPosition = new Position(Direction.WEST, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.SOUTH, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(LEFT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is l with E Direction should be directed N")
    void l_command_E_direction_end_N() {
        Position originPosition = new Position(Direction.EAST, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.NORTH, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(LEFT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }


    @Test
    @DisplayName("When received command is r with N Direction should be directed E")
    void r_command_N_direction_end_E() {
        Position originPosition = new Position(Direction.NORTH, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.EAST, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(RIGHT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is r with S Direction should be directed W")
    void r_command_S_direction_end_W() {
        Position originPosition = new Position(Direction.SOUTH, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.WEST, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(RIGHT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is r with W Direction should be directed N")
    void r_command_W_direction_end_N() {
        Position originPosition = new Position(Direction.WEST, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.NORTH, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(RIGHT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

    @Test
    @DisplayName("When received command is r with E Direction should be directed S")
    void r_command_E_direction_end_S() {
        Position originPosition = new Position(Direction.EAST, Point.ORIGIN_POINT);
        Position expectedPosition = new Position(Direction.SOUTH, Point.ORIGIN_POINT);
        Position actualPosition = Command.getExecutorFromChar(RIGHT_COMMAND).get().executeCommand(originPosition);
        assertThat(actualPosition).isEqualTo(expectedPosition);
    }

}
